<!--
  ~ Copyright (c) 2024 University of Piraeus Research Centre
  ~ 
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~ 
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~ 
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~ 
  ~ SPDX-License-Identifier: Apache-2.0
  ~ 
  ~ Contributors:
  ~     Panagiotis Karamolegkos (UPRC) - Author
-->

# Prerequisites

- A Kubernetes Cluster
- Be the Admin of the cluster, with access to the default Service Account.

### Prerequisites for Testing as a Standalone Component

- All the above Prerequisites
- Synthetic Data Generator Installed (Mandatory) [1]

### Prerequisites for Testing in CODECO

- A Kubernetes Cluster
- Be the Admin of the cluster, with access to the default Service Account.
- The rest of the CODECO Components must be installed.

# Description
The code in this repository is using all the different Images of the subcomponents from the CODECO PDLC Component. Using the bash files and a Kubernetes Cluster, an administrator can install the PDLC Subcomponents.

# Standalone Installation
Before you install PDLC you must do the following.

### Configure PDLC-DP
Go to the following YAML file:
- data_preprocessing/pdlc-dp-deployment.yaml

Write the name of the Cluster in the Enviromental Variable named: `CLUSTER_NAME`

### Configure PDLC-RL
Go to the following YAML file:
- rl_model/rl-model-deployment.yaml

Add the amount of nodes being used in the Environmental Variable named: `NODE_NUMBER`. 

### Specify the Node Name for Deployment
Specify the node that PDLC is going to be deployed on. Go in the field `spec/spec/nodeName` in the following files and provide the name of your node:
- data_preprocessing/pdlc-dp-deployment.yaml
- context_awareness/pdlc-ca-deployment.yaml
- gnn_model/gnn_controller.yaml
- gnn_model/gnn_inference.yaml
- rl_model/rl-model-deployment.yaml

### Run the Installation Scripts
Run the following commands to use the installation script:
```
chmod -R 777 apply_yamls.sh
./apply_yamls.sh
```

## How to Uninstall PDLC
Run the following commands to use the deletion script:
```
chmod -R 777 remove_yamls.sh
./remove_yamls.sh
```

# CODECO Installation
In order to install PDLC with CODECO all the rest of the CODECO components must be installed and running.

To install PDLC, the same procedure with the standalone installation needs to happen. Before running the installation scripts the correction of MDM's API URI must happen as explained below.

### Correct MDM's API URI in PDLC-DP
Go to the following YAML file:
- data_preprocessing/pdlc-dp-deployment.yaml

Change the value of the Environment Variable `MDM_URL` to the following URI:
```
"http://mdm-api.he-codeco-mdm.svc.cluster.local:8090"
```

# References
1. [Syntetic Data Generator](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/data-generators-and-datasets/synthetic-data-generator)

# Authors
- Panagiotis Karamolegkos (UPRC)
- Pepi Paraskevoulakou (UPRC)
