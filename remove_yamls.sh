# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

# chmod -R 777 remove_yamls.sh
# ./remove_yamls.sh

# Remove PDLC Containers
## Remove RL Subcomponent
kubectl delete -f ./rl_model/rl-model-deployment.yaml
kubectl delete -f ./rl_model/rl-role-binding.yaml
kubectl delete -f ./rl_model/rl-role.yaml

## Remove GNN Subcomponent
kubectl delete -f ./gnn_model/gnn_controller.yaml
kubectl delete -f ./gnn_model/gnn_inference.yaml
kubectl delete -f ./gnn_model/gnn-role-binding.yaml
kubectl delete -f ./gnn_model/gnn-role.yaml

## Remove CA Subcomponent
kubectl delete -f ./context_awareness/pdlc-ca-deployment.yaml

## Remove DP Subcomponent
kubectl delete -f ./data_preprocessing/pdlc-dp-deployment.yaml
kubectl delete -f ./data_preprocessing/pdlc-dp-role-binding.yaml
kubectl delete -f ./data_preprocessing/pdlc-dp-role.yaml

# Remove PVC for RO
kubectl delete -f ./volume/shared-pv-ro.yaml
kubectl delete -f ./volume/shared-pvc-ro.yaml

# Remove PVC for RW
kubectl delete -f ./volume/shared-pv-rw.yaml
kubectl delete -f ./volume/shared-pvc-rw.yaml

# Remove Storage Class for Persistent Volume Claim
kubectl delete -f ./volume/storage-class.yaml

# Remove the PDLC namespace
kubectl delete namespace he-codeco-pdlc
