# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

# chmod -R 777 apply_yamls.sh
# ./apply_yamls.sh

# Create the PDLC namespace
kubectl create namespace he-codeco-pdlc

# Install Storage Class for Persistent Volume Claim
kubectl apply -f ./volume/storage-class.yaml

# Install PVC for RW
kubectl apply -f ./volume/shared-pvc-rw.yaml
kubectl apply -f ./volume/shared-pv-rw.yaml

# Install PVC for RO
kubectl apply -f ./volume/shared-pvc-ro.yaml
kubectl apply -f ./volume/shared-pv-ro.yaml

# Install PDLC Containers
## Install DP Subcomponent
kubectl apply -f ./data_preprocessing/pdlc-dp-role.yaml
kubectl apply -f ./data_preprocessing/pdlc-dp-role-binding.yaml
kubectl apply -f ./data_preprocessing/pdlc-dp-deployment.yaml
# sleep 300   # Wait for Image to Download and Start
kubectl wait --for=condition=Ready pod --all -n he-codeco-pdlc --timeout=20m # 5 minutes should be enough  

## Install CA Subcomponent
kubectl apply -f ./context_awareness/pdlc-ca-deployment.yaml
# sleep 180   # Wait for Image to Download and Start
kubectl wait --for=condition=Ready pod --all -n he-codeco-pdlc --timeout=20m # 3 minutes should be enough 

## Install GNN Subcomponent
kubectl apply -f ./gnn_model/gnn-role.yaml
kubectl apply -f ./gnn_model/gnn-role-binding.yaml
kubectl apply -f ./gnn_model/gnn_inference.yaml
kubectl apply -f ./gnn_model/gnn_controller.yaml
# sleep 180   # Wait for Image to Download and Start
kubectl wait --for=condition=Ready pod --all -n he-codeco-pdlc --timeout=20m # 3 minutes should be enough 

## Install RL Subcomponent
kubectl apply -f ./rl_model/rl-role.yaml
kubectl apply -f ./rl_model/rl-role-binding.yaml
kubectl apply -f ./rl_model/rl-model-deployment.yaml
# sleep 180   # Wait for Image to Download and Start
kubectl wait --for=condition=Ready pod --all -n he-codeco-pdlc --timeout=20m # 3 minutes should be enough 